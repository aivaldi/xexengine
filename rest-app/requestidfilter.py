# -*- coding: utf-8 -*-
import logging
import uuid
from flask import Flask, request, render_template, make_response, jsonify, g, has_request_context

class RequestIdFilter(logging.Filter):
    def filter(self, record):
        '''
        This is a logging filter that makes the request ID available for use in
        the logging format. Note that we're checking if we're in a request
        context, as we may want to log things before Flask is fully loaded.
        '''
        record.request_id = self.request_id() if has_request_context() else 'No id'
        return True
    def request_id(self):
        '''
        Returns the current request ID or a new one if there is none
        In order of preference:
        If we've already created a request ID and stored it in the flask.g context local, use that
        If a client has passed in the X-Request-Id header, create a new ID with that prepended
        Otherwise, generate a request ID and store it in flask.g.request_id
        '''
        if getattr(g, 'request_id', None):
            return g.request_id
        headers = request.headers
        original_request_id = headers.get("X-Request-Id")
        new_uuid = self.generate_request_id(original_request_id)
        g.request_id = new_uuid
        return new_uuid
    def generate_request_id(self, original_id=''):
        '''
        Generate a new request ID, optionally including an original request ID
        '''
        new_id = uuid.uuid4()
        if original_id:
            new_id = "{},{}".format(original_id, new_id)
        return new_id