from flask_restplus import Namespace, fields


class SourcesDto:
    api = Namespace('resources', description='Get Sources to connect with')
    sources = api.model('resources', {
        'name': fields.String(required=True, description='resources name'),
        'code': fields.String(required=True, description='resources code must be unique'),
        'type': fields.String(required=True, description='resources type of'),
        'parameters': fields.String(description='user Identifier')
    })