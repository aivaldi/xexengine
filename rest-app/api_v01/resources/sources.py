from flask import request
from flask_restplus import Resource
from ..dtos import SourcesDto

api = SourcesDto.api
_sources = SourcesDto.sources


@api.route('/')
class SourceList(Resource):
    @api.doc('list_of_registered_users')
    @api.marshal_list_with(_sources, envelope='data')
    def get(self):
        """List all registered users"""
        return [{}]

    @api.response(201, 'User successfully created.')
    @api.doc('create a new user')
    @api.expect(_sources, validate=True)
    def post(self):
        """Creates a new User """
        data = request.json
        return data


@api.route('/<public_id>')
@api.param('public_id', 'The User identifier')
@api.response(404, 'User not found.')
class User(Resource):
    @api.doc('get a user')
    @api.marshal_with(_sources)
    def get(self, public_id):
        """get a user given its identifier"""
        user = [{}]