# -*- coding: utf-8 -*-

import os
from flask import Blueprint
from flask_restplus import Api


from .resources.sources  import api as sources_ns

bp = Blueprint('api_v01', __name__, url_prefix='/fdm/api/v0.2')
api_v01 = Api(bp,
          title='FLASK RESTPLUS API BOILER-PLATE WITH JWT',
          version='1.0',
          description='a boilerplate for flask restplus web service'
          )

api_v01.add_namespace(sources_ns, path='/sources')
