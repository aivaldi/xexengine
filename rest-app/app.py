# -*- coding: utf-8 -*-
import logging, sys, os, time
from flask import Flask, request, make_response, jsonify, g

sys.path.append('../')

from requestidfilter import RequestIdFilter
from extensions import auth
from api_v01 import bp

from lib_old import Log

from lib_old import Config

__all__ = ['create_app']

DEFAULT_BLUEPRINTS = (
    bp,
)


def create_app(config=None, app_name=None, blueprints=None, serve_mode=None):
    """Create a Flask app."""
    if serve_mode not in ['wsgi']:  # If not behind gunicorn wsgi gateway
        Log.SetLoggers()
    config = Config()
    config.loadConfigs(path='../etc')

    if app_name is None:
        app_name = "fdm-rest-api_v01"
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS

    app = Flask(app_name)
    configure_app(app, config)
    configure_blueprints(app, blueprints)
    configure_extensions(app)
    configure_logging(app)
    configure_hook(app)

    app.app_context().push()
    return app


def configure_app(app, config=None):
    config_flask_items = Config().getItemsOfSectionAsDict('flask')
    # flask likes its vars uppercase
    for k, v in config_flask_items.items():
        if v in ['true', 'True', 'yes', 'Yes']: v = True
        if v in ['false', 'False', 'no', 'No']: v = False
        app.config[k.upper()] = v


def configure_extensions(app):
    @auth.get_password
    def get_password(username):
        if username == 'XeN':
            return 'XeN'
        return None

    @auth.error_handler
    def unauthorized():
        app.logger.info("Unauthorized Access")
        return make_response(jsonify({'message': 'Unauthorized access'}), 401)


def configure_blueprints(app, blueprints):
    """Configure blueprints in views."""
    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def configure_logging(app):
    """Configure logging."""
    logggers_whith_request_id = ['rest-api_v01', 'fdm-core', 'drivers', 'snmp', 'telnet']
    for l in logggers_whith_request_id:
        logging.getLogger(l).addFilter(RequestIdFilter())
    for h in logging.getLogger('rest-api_v01').handlers:
        app.logger.addHandler(h)
    app.logger.debug("Starting Flask Application")


def configure_hook(app):
    @app.before_request
    def before_request():
        g.start = time.time()
        app.logger.info('\t'.join([
            request.remote_addr,
            request.method,
            request.url]))

    @app.after_request
    def after_request(response):
        app.logger.debug('Response code: %s', str(response.status))
        return response

