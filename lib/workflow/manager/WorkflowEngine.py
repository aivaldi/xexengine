
from lib.log.log import Log
class WorkflowManager:

    def __init__(self):
        '''
        Workflow engine main class
        Control all workflow execution
        '''
        self.log = Log.getLogger()
        self.log.info("WorkflowManager created")
