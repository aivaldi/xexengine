from lib.xenobject import XeNObject


class WorkflowParser (XeNObject):
    '''
    Parse the worflow structure from file to
    Workflow Structure
    '''

    def parseJson(self, json):
        '''
        Parse JSON and return WorkflowStructure
        @:param json: Json object
        :return: Parsed element
        '''
        self.log.debug("WorkflowParser BEGIN")
        self.log.debug("WorkflowParser END")
        pass
