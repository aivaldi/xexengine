import os, logging, glob
from configparser import ConfigParser


class Singleton(object):
    _instances = {}

    def __new__(class_, *args, **kwargs):
        if class_ not in class_._instances:
            class_._instances[class_] = super(Singleton, class_).__new__(class_, *args, **kwargs)
        return class_._instances[class_]


class Config(Singleton):
    parser = None

    def loadConfigs(self, path=''):
        log = logging.getLogger('fdm-core')
        log.info("Start loading configs")
        if Config.parser is None:
            Config.parser = ConfigParser()

            if path == '':
                files_to_parce = glob.glob(os.path.join(os.path.dirname(__file__), '../../etc/*.ini'))
            else:
                files_to_parce = glob.glob(os.path.join(path, '*.ini'))

            for file in files_to_parce:
                Config.parser.read(file)

    def getConf(self, section=None):
        if section and section in Config.parser.sections():
            return Config.parser[section]
        elif section == 'all':
            return Config.parser
        else:
            raise Exception("Section {0} must be present in config file or config file not found".format(section))

    def getItemsOfSectionAsDict(self, section):
        if section in Config.parser.sections():
            return dict(Config.parser[section])
        else:
            raise Exception("Section {0} must be present in config file".format(section))

