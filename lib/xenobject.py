from lib.log.log import Log


class XeNObject:
    '''
    Basic XeN Object for all clases core
    '''
    initialized = False

    @staticmethod
    def initialize():
        if not XeNObject.initialized:
            XeNObject.initialized = True
            Log.SetLoggers('xen-core')

    def __init__(self):
        XeNObject.initialize()
        self.log = Log.getLogger('xen-core')
