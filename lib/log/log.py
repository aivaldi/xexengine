import os
import logging
import logging.config

class Log(object):
    @staticmethod
    def SetLoggers(logName='root'):
        path_logging_config_ini = os.path.join(os.path.dirname(__file__), '../../etc/loggin_config.ini')
        logging.config.fileConfig(path_logging_config_ini)
        print(logName)
        log = logging.getLogger(logName)
        log.info("Logger test_engine {} started".format(logName))

    @staticmethod
    def getLogger(logName='root'):
        return logging.getLogger(logName);
