import unittest

class TestEngineBasicMethods(unittest.TestCase):

    def test_csv_access(self):
        self.assertEqual('foo'.upper(), 'FOO')


if __name__ == '__main__':
    unittest.main()